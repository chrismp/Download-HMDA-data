import sys, json, csv
from time import sleep

def addLeadingZeroes(intGeoID,requiredLength):
	strGeoID = str(intGeoID)
	while len(strGeoID) < requiredLength:
		strGeoID = '0'+strGeoID
	return strGeoID	

jsonData = json.load(open(sys.argv[1]))
outputFile = open(sys.argv[2],'w')
csvwriter = csv.writer(outputFile)

count = 0
for result in jsonData["results"]:
	if "census_tract_number" in result:
		result["census_tract_number"] = result["census_tract_number"].replace('.','')
		result["tract"] = result.pop("census_tract_number")

	print result
	result["county_code"] = addLeadingZeroes(result["county_code"],3)
	result["county"] = result.pop("county_code")
	result["state_code"] = addLeadingZeroes(result["state_code"],2)
	result["state"] = result.pop("state_code")

	if count==0:
		header = result.keys()
		csvwriter.writerow(header)
		count += 1
	csvwriter.writerow(result.values())
outputFile.close()