URL for querying HMDA LAR slices: https://api.consumerfinance.gov/data/hmda/slice/hmda_lar.html

Get ZCTA5-Tract relation file: https://www.census.gov/geo/maps-data/data/relationship.html

DO NOT TRY TO ADD UP CENSUS TRACTS TO ZCTA5 BOUNDARIES: http://guides.ucf.edu/c.php?g=78401&p=516944